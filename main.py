
import random
# On importe le package random pour mélanger

users_list = [["Pascal", "ROSE", 43],["Mickaël", "FLEUR", 29],["Henri", "TULIPE", 35],["Michel", "FRAMBOISE", 35],["Arthur", "PETALE", 35],["Michel", "POLLEN", 50],["Maurice", "FRAMBOISE", 42]]
# On creer les listes utilisateurs
random.shuffle(users_list)
# On mélange la liste


def noms_ordre_alphabetique():
    """
    La fonction creer une liste des noms par ordre alphabetique
    """
    list_noms = []
    # creer une liste vide
    for user in users_list:
        # boucle les utilisateur dans la list utilisateur
        nom = user[1]
        # les noms sont placé en 2e position dans la liste
        list_noms.append(nom)
        # ajouter nom dans la liste list_noms
    list_noms.sort(key=lambda x: [ord(c) for c in x])
    # Trier la liste de noms avec les valeurs ASCII
    print("## Liste 1 : tri par ordre alphabétique des noms ##")
    # affiche la liste 1
    for ligne in sorted(users_list, key=lambda x: x[1]):
    # Boucle a chaque trie du 2e éléments de la liste
        print(ligne)
        #affiche le trie


noms_ordre_alphabetique()


def prenoms_ordre_alphabetique():
    """
    La fonction creer une liste des prenoms par ordre alphabetique
    """
    list_prenoms = []
    for user in users_list:
        prenom = user[0]
        list_prenoms.append(prenom)
    list_prenoms.sort(key=lambda x: [ord(c) for c in x])
    print("## Liste 2 : tri par ordre alphabétique inversé des prenoms ##")
    for ligne in sorted(users_list, key=lambda x: x[0]):
        print(ligne)

prenoms_ordre_alphabetique()


def age_croissant():
    """
    La fonction creer une liste des ages par ordre croissant
    """
    list_ages = []
    for user in users_list:
        age = user[2]
        list_ages.append(age)
    list_ages.sort()
    print("## Liste 3 : tri par ordre croissant des âges ##")
    for ligne in sorted(users_list, key=lambda x: x[2]):
        print(ligne)

age_croissant()


def noms_et_prenoms_ordre_alphabetique():
    """
    La fonction creer une liste des noms par ordre alphabetique et si 2 noms identique se base sur les prenoms
    """
    def cle_tri(user):
        nom = user[1]
        prenom = user[0]
        return (nom, prenom)

    users_list.sort(key=cle_tri)

    print("## Liste 4 : tri par nom (et prénom en cas de noms identiques) ##")
    for ligne in users_list:
        print(ligne)


noms_et_prenoms_ordre_alphabetique()

